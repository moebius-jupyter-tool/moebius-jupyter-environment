import json
import urllib.request
from os import listdir
from os.path import isfile

base_schema_dir = "schemas"
base_url = "http://0.0.0.0:8081"


def schema_exists(schema_name: str) -> bool:
    """Check if schema already exists."""
    url = f"{base_url}/subjects"
    request = urllib.request.Request(url=url, method="GET")
    response = urllib.request.urlopen(request)
    all_schemas_names = json.loads(response.read())
    return schema_name in all_schemas_names


def register_new_schema(schema: dict) -> None:
    """Register schema."""
    headers = {"Content-Type": "application/vnd.schemaregistry.v1+json"}
    url = f"{base_url}/subjects/{schema['name']}/versions"
    raw_data = {"schema": json.dumps(schema)}
    data = json.dumps(raw_data).encode("utf-8")

    request = urllib.request.Request(url=url, headers=headers, method="POST", data=data)
    urllib.request.urlopen(request)


for schema_file in listdir(base_schema_dir):
    path = f"{base_schema_dir}/{schema_file}"
    if isfile(path):
        with open(path, "r") as file:
            schema_content = json.load(file)

            if not schema_exists(schema_content["name"]):
                register_new_schema(schema_content)
